//
// Created by rober on 10.10.2020.
// how to install under:
// https://linuxize.com/post/how-to-install-opencv-on-ubuntu-20-04/
//

#ifndef _CAPTURE_H
#define _CAPTURE_H
#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>

class Capture {
public:
	Capture();
	~ Capture();

	void captureVideo();
	std::string exec(const char* cmd);

};



#endif //_CAPTURE_H
