//
// Created by rober on 10.10.2020.
//

#include "Sending.h"
#include <fstream>
#include <iostream>


bool Sending::sendData() {

	std::ifstream fin("C:\\myfile.zip", ios::binary);

	while (fin) {
		constexpr size_t max_xfer_buf_size = 10240;
		char buffer[max_xfer_buf_size];
		fin.read(buffer,
					 sizeof(buffer));
		if (fin.gcount()
			> 0) {
			ssize_t nwritten = sftp_write(NULL, buffer, fin.gcount());
			if (nwritten != fin.
					gcount()
					) {
				fprintf(stderr, "Can't write data to file: %s\n", ssh_get_error(ssh_session)
				);
				sftp_close(file);
				return 1;
			}
		}
	}
}