file(REMOVE_RECURSE
  "AVCapture.exe"
  "AVCapture.exe.manifest"
  "AVCapture.pdb"
  "CMakeFiles/AVCapture.dir/Capture.cpp.obj"
  "CMakeFiles/AVCapture.dir/Sending.cpp.obj"
  "CMakeFiles/AVCapture.dir/Temperature.cpp.obj"
  "CMakeFiles/AVCapture.dir/main.cpp.obj"
  "libAVCapture.dll.a"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/AVCapture.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
